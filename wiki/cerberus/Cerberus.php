<?php 

class Cerberus
{
	public function addHead($data)
	{
		$id = $data['slug'];
		$name = $data['name'];

		// Create Symlink
		$target = $_SERVER['DOCUMENT_ROOT'] . '/wiki';
		$link =  $_SERVER['DOCUMENT_ROOT'] . '/wikis/' . $id;
		symlink($target, $link);
		
		// Create Database
		// Get Data From LocalSettings.php
		include  $_SERVER['DOCUMENT_ROOT'] . '/wiki/LocalSettings.php';

		$servername = $wgDBserver;
		$username = $wgDBuser;
		$password = $wgDBpassword;

		// Create connection
		$conn = new mysqli($servername, $username, $password);
		// Create database
		$dbName = "cerberus-" . $id;
		$sql = "CREATE DATABASE `" . $dbName . "`";
		$conn->query($sql);

		// Import Database Template
		$sqlSource = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/wiki/cerberus/template.sql');

		$conn->select_db($dbName);

		mysqli_multi_query($conn, $sqlSource);

		$conn = new mysqli($servername, $username, $password);

		$status = $conn->select_db('cerberus');

		$sql = "CREATE TABLE IF NOT EXISTS family_map (id INT(25) UNSIGNED AUTO_INCREMENT PRIMARY KEY,name VARCHAR(128) NOT NULL, external_id INT(25) NOT NULL, database_name VARCHAR(128) NOT NULL, subdirectory VARCHAR(128) NOT NULL, logo_url VARCHAR(1024) NOT NULL  )";

		$status = $conn->query($sql);

		$conn = new mysqli($servername, $username, $password);

		$status = $conn->select_db('cerberus');

		$logo_url = $data['logo_url'];
		$external_id = $data['id'];

		$sql = "INSERT INTO family_map (name, external_id, database_name, subdirectory, logo_url) VALUES (\"$name\", $external_id, \"$dbName\", \"$id\", \"$logo_url\")";

		$status = $conn->query($sql);

		$response = [
			"status" => "success"
		];

		echo json_encode($response);
	}
	

	public function modifyHead($data)
	{
		$id = $data['slug'];
		$name = $data['name'];

		$external_id = $_SERVER["HTTP_X_EXTERNAL_ID"];

		// Create Database
		// Get Data From LocalSettings.php
		include  $_SERVER['DOCUMENT_ROOT'] . '/wiki/LocalSettings.php';

		$conn = new mysqli($wgDBserver, $wgDBuser, $wgDBpassword);
		$conn->select_db('cerberus');		

		$map_row = $conn->query("SELECT * FROM family_map where external_id = $external_id")->fetch_array();

		$wgDBname = $map_row['database_name'];
		$subdirectory = $map_row['subdirectory'];

		// Create Symlink
		$current_directory =  $_SERVER['DOCUMENT_ROOT'] . '/wikis/' . $subdirectory;
		$new_directory = $_SERVER['DOCUMENT_ROOT'] . '/wikis/' . $id;
		rename($current_directory, $new_directory);

		// Create connection
		$conn = new mysqli($wgDBserver, $wgDBuser, $wgDBpassword);

		$status = $conn->select_db('cerberus');

		$logo_url = $data['logo_url'];
		$external_id = $data['id'];

		$sql = "UPDATE family_map SET name=\"$name\", subdirectory=\"$id\", logo_url=\"$logo_url\" WHERE external_id=$external_id";

		$status = $conn->query($sql);

		$response = [
			"status" => "success"
		];

		echo json_encode($response);
	}


	public function decapitateHead()
	{
		// Remove Database


	}
}